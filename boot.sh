#!/bin/bash
set -e

# Sanity check
[[ -d linux ]] || { echo Missing kernel source; exit 1; }
[[ -d buildroot ]] || { echo Missing buildroot source; exit 1; }

# Compile kernel
pushd linux
make ARCH=arm64 LLVM=1 -j$(nproc)
popd

# Check for needed files
[[ -f linux/arch/arm64/boot/Image.gz ]] || { echo Missing linux/arch/arm64/boot/Image.gz; exit 1; }
[[ -f linux/arch/arm64/boot/dts/nvidia/tegra132-flounder.dtb ]] || { echo Missing linux/arch/arm64/boot/dts/nvidia/tegra132-flounder.dtb; exit 1; }
[[ -f buildroot/output/images/rootfs.cpio.xz ]] || { echo Missing buildroot/output/images/rootfs.cpio.xz; exit 1; }

# Create boot.img
[[ -d output ]] || mkdir output
cat linux/arch/arm64/boot/Image.gz linux/arch/arm64/boot/dts/nvidia/tegra132-flounder.dtb > output/zImage
abootimg --create output/boot.img -f bootimg.cfg -k output/zImage -r buildroot/output/images/rootfs.cpio.xz

# Boot boot.img
fastboot boot output/boot.img
